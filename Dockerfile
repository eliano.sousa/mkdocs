FROM python:3.9.18

RUN pip3 install mkdocs
COPY mkdocs /opt/mkdocs
WORKDIR /opt/mkdocs
RUN pwd && sleep 15 && ls -la && mkdocs -V
RUN mkdocs build -f mkdocs.yml
EXPOSE 8081
ENTRYPOINT ["mkdocs"]
CMD ["serve", "--dev-addr=0.0.0.0:8081"]

